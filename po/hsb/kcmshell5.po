# translation of desktop_kdebase.po to Upper Sorbian
# Prof. Dr. Eduard Werner <e.werner@rz.uni-leipzig.de>, 2003.
# Eduard Werner <edi.werner@gmx.de>, 2005, 2008.
# Bianka Šwejdźic <hertn@gmx.de>, 2005, 2007.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdebase\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-29 00:46+0000\n"
"PO-Revision-Date: 2008-11-06 22:08+0100\n"
"Last-Translator: Eduard Werner <edi.werner@gmx.de>\n"
"Language-Team: en_US <kde-i18n-doc@lists.kde.org>\n"
"Language: hsb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: main.cpp:183
#, kde-format
msgid "System Settings Module"
msgstr ""

#: main.cpp:185
#, kde-format
msgid "A tool to start single system settings modules"
msgstr ""

#: main.cpp:187
#, kde-format
msgid "(c) 1999-2016, The KDE Developers"
msgstr ""

#: main.cpp:189
#, kde-format
msgid "Frans Englich"
msgstr ""

#: main.cpp:189
#, kde-format
msgid "Maintainer"
msgstr ""

#: main.cpp:190
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: main.cpp:191
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr ""

#: main.cpp:192
#, kde-format
msgid "Matthias Elter"
msgstr ""

#: main.cpp:193
#, kde-format
msgid "Matthias Ettrich"
msgstr ""

#: main.cpp:194
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:200
#, kde-format
msgid "List all possible modules"
msgstr ""

#: main.cpp:201
#, kde-format
msgid "Configuration module to open"
msgstr ""

#: main.cpp:202
#, kde-format
msgid "Specify a particular language"
msgstr ""

#: main.cpp:203
#, kde-format
msgid "Do not display main window"
msgstr ""

#: main.cpp:204
#, kde-format
msgid "Arguments for the module"
msgstr ""

#: main.cpp:205
#, kde-format
msgid "Use a specific icon for the window"
msgstr ""

#: main.cpp:206
#, kde-format
msgid "Use a specific caption for the window"
msgstr ""

#: main.cpp:215
#, kde-format
msgid ""
"--lang is deprecated. Please set the LANGUAGE environment variable instead"
msgstr ""

#: main.cpp:219
#, kde-format
msgid "The following modules are available:"
msgstr ""

#: main.cpp:239
#, kde-format
msgid "No description available"
msgstr "Žane wopisanje namakał"

#: main.cpp:303
#, kde-format
msgid ""
"Could not find module '%1'. See kcmshell5 --list for the full list of "
"modules."
msgstr ""
